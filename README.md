# Purpose
Repository is for openwrt linux distribution custom compilation and configuration.

Target hardware is tplink wrd3600
https://oldwiki.archive.openwrt.org/toh/tp-link/tl-wdr3600

# Build

Command to download openwrt source code then build openwrt image sdk and uboot is
```
./build
```

Openwrt images will be created in path
```
$ ls -1 ./openwrt/out/bin/targets/ar71xx/generic
config.seed
openwrt-ar71xx-generic-device-tl-wdr3600-v1.manifest
openwrt-ar71xx-generic-root.squashfs
openwrt-ar71xx-generic-tl-wdr3600-v1-squashfs-factory.bin
openwrt-ar71xx-generic-tl-wdr3600-v1-squashfs-sysupgrade.bin
openwrt-ar71xx-generic-uImage-lzma.bin
openwrt-ar71xx-generic-vmlinux-lzma.elf
openwrt-ar71xx-generic-vmlinux.bin
openwrt-ar71xx-generic-vmlinux.elf
openwrt-ar71xx-generic-vmlinux.lzma
packages
sha256sums
```

Uboot images will created in path
```
$ ls -1 ./uboot/out/bin/
README
u-boot_mod__tp-link_tl-wdr3600_v1__20190713__git_master-7a540a78.bin
u-boot_mod__tp-link_tl-wdr3600_v1__20190713__git_master-7a540a78.md5
```

INFO: Next time build command will skip downloading source code and rebuilds only.

INFO: All dependencies must be installed in host system as described in

https://openwrt.org/docs/guide-developer/quickstart-build-images

## Docker

Alternatively for build process it could be used docker. First time it is needed to build docker image
```
./docker/build
```
Then run build in new docker container
```
./docker/run
```

To shell into such container
```
./docker/run bash
```

# Configuration

Build step run first time invokes ./openwrt/fetch script which fetch sources and set custom configuration from this repository. To customize it more do:
* go to openwrt build directory
```
cd ./openwrt/out/
```
* run configuration tool, modify some options and save your changes
```
make menuconfig
```
* then copy config diff into this repository to track it under git
```
./scripts/diffconfig.sh > ../diffconfig
```

# Flash

# Website

Go to router website and flash new image using menu option

http://192.168.1.1

If it is vendor tplink website then flash image named openwrt-ar71xx-generic-tl-wdr3600-v1-squashfs-factory.bin.

If it is openwrt upgrade then use file file openwrt-ar71xx-generic-tl-wdr3600-v1-squashfs-sysupgrade.bin.

https://openwrt.org/docs/guide-quick-start/sysupgrade.luci

## Ssh

Run in downloaded artifacts directory
```
./openwrt/upgrade
```

https://openwrt.org/docs/guide-user/installation/generic.sysupgrade

## Direct flash write

This method rewrites all flash memory. Comparing to sysupgrade not only opewrt partition is updated, but all of them. Then uboot also will be replaced with custom one. Extra firmware data on the last partition is replaced by backup of my own device.

During flashing main processor have to be in reset state to not interfere with memory bus. This can be done by grounding the right side of the R2 pad (the resistor is not populated on the board I have), near pin 11 on the JTAG header (the 2×7 pin header at J1).

Use ch341a spi flash programmer

Connect wires with 3.3 volt logic

https://www.pjrc.com/store/w25q64fv.pdf

```
Programmer - Flash
CS         - CS
MISO       - DO
WP         - 1
GND        - GND
VCC        - VCC
HOLD       - 1
CLK        - CLK
MOSI       - DI
```

Run command
```
./image/flash
```

https://openwrt.org/docs/techref/flash.layout

# Install

Optional installation of custom configuration into target device
```
./openwrt/install
```

It requires to setup password storage. Secrets should be set as in templates in ./secret directory.

https://linux.die.net/man/1/pass

# Extended root

Optional external disk or pendrive can be used to extend internal openwrt flash storage and overlay this filesystem.

* Run install script

* Prepare pendrive partitions on host machine:

```
./openwrt/extroot/initialize /dev/sd<X>
```

* Insert pendrive into target openwrt device

* Reboot target router

* Install extra packages
```
./openwrt/extroot/install
```

# Recovery

Hard reboot and press few times wps button for 1 second. Then it should boot rescue image with open ssh access.

https://openwrt.org/docs/guide-user/troubleshooting/vendor_specific_rescue


# Uboot

Follow instructions

https://github.com/pepe2k/u-boot_mod

Upload new bootloader
```
scp uboot/out/bin/u-boot_mod__tp-link_tl-wdr3600_v1__20200126__git_master-*.bin root@192.168.1.1:/tmp/
```

Save existing bootloader
```
root@OpenWrt:~# cat /dev/mtd0 > /tmp/uboot_factory.bin
```

Extract last bytes from factory bootloader
```
root@OpenWrt:~# dd if=/tmp/uboot_factory.bin of=/tmp/uboot_rest.bin bs=1 skip=$(wc -c < /tmp/uboot_facto
ry.bin)
0+0 records in
0+0 records out
```

Merge both
```
root@OpenWrt:~# cat /tmp/u-boot_mod__tp-link_tl-wdr3600_v1__20200126__git_master-7a540a78.bin /tmp/uboot
_rest.bin > /tmp/uboot_new.bin
```

Free ram
```
root@OpenWrt:~# rm /tmp/uboot_factory.bin
root@OpenWrt:~# rm /tmp/uboot_rest.bin
```

Flash it
```
root@OpenWrt:/tmp# mtd write /tmp/uboot_new.bin  u-boot
Unlocking u-boot ...

Writing from /tmp/uboot_new.bin to u-boot ...
```

**NOTICE:** If there is error that writing is not permitted, then apply patch and recompile
```
k@legion 0 18:01:27 ~/project/setup-openwrt-tplink-wdr3600/openwrt/out
% git diff
diff --git a/target/linux/ar71xx/files/drivers/mtd/tplinkpart.c b/target/linux/ar71xx/files/drivers/mtd/tplinkpart.c
index 1b94163b83..1e518d1c20 100644
--- a/target/linux/ar71xx/files/drivers/mtd/tplinkpart.c
+++ b/target/linux/ar71xx/files/drivers/mtd/tplinkpart.c
@@ -154,7 +154,7 @@ static int tplink_parse_partitions_offset(struct mtd_info *master,
        parts[0].name = "u-boot";
        parts[0].offset = 0;
        parts[0].size = offset;
-       parts[0].mask_flags = MTD_WRITEABLE;
+       parts[0].mask_flags = 0;

        parts[1].name = "kernel";
        parts[1].offset = offset;
````
