#!/usr/bin/awk -f

# Parse dnsmasq log and print it nicer
#
# Usage: tail -f /var/log/dnsmasq.log | /usr/bin/dnsmasq-log-parse.awk
# Log dump have to be triggered: kill -sigusr1 <dnsmasq pid>
#
# https://superuser.com/questions/632898/how-to-log-all-dns-requests-made-through-openwrt-router

BEGIN {
  OFS = ",";
}

$5 == "query[A]" {
  time = mktime( \
    sprintf("%04d %02d %02d %s\n", \
      strftime("%Y", systime()), \
      (match("JanFebMarAprMayJunJulAugSepOctNovDec",$1)+2)/3, \
      $2, \
      gensub(":", " ", "g", $3) \
    ) \
  );
  query = $6;
  host = $8;
  print time, host, query;
}
